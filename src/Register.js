import { useState } from 'react';
import {Link, useHistory} from 'react-router-dom';

import axiox from "axios";

const Register = () => {
    
    const [poid, setPoid] = useState("");
    const [genre, setGenre] = useState("");
    const [age, setAge] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const[error, setError] = useState("");
    let history = useHistory();
    
    
    const register = (e) => {
        
       e.preventDefault();     
       axiox.post("http://localhost:5000/api/auth/register",{
       email,
       password,
       poid,
       genre,
       age

    }).then((response) => {
        
        localStorage.setItem("login",JSON.stringify({
            userLogin: true,
            token: response.data.access_token
        })
        );
        setError("");
        setEmail("");
        setPassword("");
        setAge("");
        setGenre("");
        setPoid("");
        history.push("/");
        
    }).catch(error => setError(error.response.data.message));
    }
    

    return ( 
        <div className="Login">


            {error && <p style={{color: "red"}}>{error}</p>}



             <form onSubmit={register} className="form">
             <div className="form-group">
            
            
            
            
            <label className='test2' htmlFor="Nom" >Email</label>
             <input 
            type="text" 
            className="form-control" 
            id="nom"
            value={email}
            onChange={(e)=> setEmail(e.target.value)}
            required />
            
            
            <label className='test2' htmlFor="Password">Mot de passe</label>
             <input 
            type="Password" 
            className="form-control" 
            id="password"
            value={password}
            onChange={(e)=>setPassword(e.target.value)}
            required />


            <label className='test2' htmlFor="Nom">Poids actuelle</label>
             <input 
            type="text" 
            className="form-control" 
            id="poid"
            value={poid}
            onChange={(e)=> setPoid(e.target.value)}
            required />


            <label className='test2' htmlFor="Nom">Age</label>
            <input 
            type="text" 
            className="form-control" 
            id="age"
            value={age}
            onChange={(e)=> setAge(e.target.value)}
            required />
           


            <label className='test2' htmlFor="Nom">Genre</label>
            <select 
            required 
            className="form-control"
            id="genre" 
            value={genre} 
            onChange={ (e)=> setGenre(e.target.value) }>
            <option value=""></option>
            <option value="Masculin">Masculin</option>
            <option value="Féminin">Féminin</option>
            </select>

            
           
            
            
            
             </div>
             <button  type="submit" className="btn-create">S'inscrire</button>
             <Link   to="/" className="lien">Nouveau ? Creer un Compte !</Link>
             </form>

        </div>
     );
}
 
export default Register;